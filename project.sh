#!/bin/bash

#Get input for the moment to search tweets from
echo Type the year you want to search
read YEAR

echo Type the month you want to search
read MONTH

echo Type the day you want to search
read DAY

#Get input for the username you want to look up
echo Type a username
read USER

#Find tweets with question marks in them and the given username in every hour on the given date
for i in {00..23}
do
    zless /net/corpora/twitter2/Tweets/$YEAR/$MONTH/$YEAR$MONTH$DAY\:$i.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | grep -i @$USER | grep ? | wc -l >> $USER.tweetnumber
done
