#!/bin/bash

echo 'there are this many tweets on March 1st at 12 PM:'
#accessing the files in the directory of March 1st 2017 at 12 PM by zless, showing the text from tweets using the tweet2tab tool and omiting errors with -i, count the number of lines with wc -l
zless /net/corpora/twitter2/Tweets/2017/01/20170112\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l

echo 'there are this many unique tweets:'
#accessing the files in the directory of March 1st 2017 at 12 PM by zless, showing the text from tweets using the tweet2tab tool and omiting errors with -i, omit duplicate tweets using awk, count the number of lines  with wc -l
zless /net/corpora/twitter2/Tweets/2017/01/20170112\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' |  wc -l

echo 'out of that number there are this many retweets:'
#accessing the files in the directory of March 1st 2017 at 12 PM by zless, showing the text from tweets using the tweet2tab tool and omiting errors with -i, omit duplicate tweets using awk, find retweets with grep, count the number of lines with wc -l
zless /net/corpora/twitter2/Tweets/2017/01/20170112\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | grep '^RT @' | wc -l

echo 'these are the first twenty unique tweets that are not retweets:'
#accessing the files in the directory of March 1st 2017 at 12 PM by zless, showing the text from tweets using the tweet2tab tool and omiting errors with -i, omit duplicate tweets using awk, show only tweets that are not retweets using grep, count the number of lines with wc -l
zless /net/corpora/twitter2/Tweets/2017/01/20170112\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab text -i | awk '!x[$0]++' | grep -v '^RT @' | head -20
